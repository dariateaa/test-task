﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace TestTask
{
    class Game
    {
        // Объект игрока "Компьютер".
        Player _computer;
        // Объект игрока.
        Player _player;
        // Флаг автоигры.
        bool _autoplay;
        Random _rand;
        
        // Конструктор, принимающий в качестве параметра имя игрока.
        public Game(string playerName)
        {
            _computer = new Player("Computer");
            _player = new Player(playerName);
            _autoplay = true;
            _rand = new Random();
        }

        // Метод - меню игры, вызывается в main. 
        public void Menu()
        {
            Console.Clear();
            centerText("1. Нaчать игру");
            centerText("2. Выход");
            Console.Write("Ваш выбор: ");
            int answ = Int32.Parse(Console.ReadLine());
            Console.Clear();
            switch (answ)
            {
                case 1:
                    centerText("1. Режим auto");
                    centerText("2. Ручное управление");
                    centerText("3. Выход");
                    Console.Write("Ваш выбор: ");
                    answ = Int32.Parse(Console.ReadLine());
                    Console.Clear();
                    switch (answ)
                    {
                        case 1:
                            // _autoplay по-умолчанию - true. Вызываем метод Play - начало игры.
                            Play();
                            break;
                        case 2:
                            // Включаем ручной режим, установив флаг _autoplay значение false.
                            _autoplay = false;
                            Play();
                            break;
                        case 3:
                            break;
                        default:
                            break;
                    }
                    break;
                case 2:
                    break;
                default:
                    break;

            }            
        }

        // Метод центрирования текста. Используется для оформления меню.
        private static void centerText(String text)
        {
            Console.Write(new string(' ', (Console.WindowWidth - text.Length) / 2));
            Console.WriteLine(text);
        }

        // Основной метод игры.
        private void Play()
        {
            int whoPlays, step;
            // Пока здоровье одного из игроков > 0, игра продолжается
            while (_computer.getHealth() > 0 && _player.getHealth() > 0)
            {
                // Рандомно выбирается, кто делает текущий ход
                whoPlays = _rand.Next(1, 3);
                switch (whoPlays)
                {
                    case 1:
                        System.Threading.Thread.Sleep(100);
                        Console.WriteLine();
                        Console.ForegroundColor = ConsoleColor.Cyan;
                        Console.Write(_computer.getName());
                        Console.ResetColor();
                        Console.WriteLine(" делает свой ход...");
                        System.Threading.Thread.Sleep(1000);
                        // Если здоровье компьютера <= 35 - вероятность на исцеление повышается 
                        if (_computer.getHealth() <= 35)
                            // Рандомно выбираем ход.
                            step = _rand.Next(1, 5);
                        else
                            step = _rand.Next(1, 4);
                        // Вызов метода, который "совершает ход", принимает объект текущего игрока 
                        // и что именно нужно сделать.
                        makeChoice(_computer, step);
                        break;
                    case 2:
                        System.Threading.Thread.Sleep(100);
                        Console.WriteLine();
                        Console.ForegroundColor = ConsoleColor.Magenta;
                        Console.Write(_player.getName());
                        Console.ResetColor();
                        Console.WriteLine(" делает свой ход...");
                        System.Threading.Thread.Sleep(1000);
                        // Если авторежим отключен, управление передается игроку. 
                        if (_autoplay == false)
                        {
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.WriteLine("\n\t1. Урон в диапазоне 10-35.");
                            Console.WriteLine("\n\t2. Урон в диапазоне 18-25.");
                            Console.WriteLine("\n\t3. Исцеление в диапазоне 18-25.");
                            while (true)
                            {
                                Console.Write("Ваш выбор: ");
                                step = Int32.Parse(Console.ReadLine());
                                if (step >= 1 && step <= 3)
                                    break;
                                else
                                    Console.WriteLine("Введите число от 1 до 3!");
                            }
                            Console.ResetColor();
                        }
                        // Если авторежим включен и здоровье игрока <= 35 - повышается шанс на исцеление
                        else if (_player.getHealth() <= 35)
                            step = _rand.Next(1, 5);
                        else
                            step = _rand.Next(1, 4);
                        makeChoice(_player, step);
                        break;
                }
            }
            System.Threading.Thread.Sleep(1500);
            // Определение победителя и вывод сообщения в консоль
            if (_player.getHealth() > 0)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine();
                centerText("ВЫ ВЫИГРАЛИ!");
                Console.ResetColor();
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine();
                centerText("Вы проиграли..");
                Console.ResetColor();
            }

        }
        
        // Метод, который "делает ход"
        private void makeChoice(Player obj, int step)
        {
            switch (step)
            {
                case 1:
                    // Если ход делает игрок - наносим урон(в максимальном диапазоне, 10-35) компьютеру, иначе - игроку.
                    if(obj.getName()==_player.getName())
                        _computer.Damage();
                    else
                        _player.Damage();
                    break;
                case 2:
                    // Если ход делает игрок - наносим урон(в минимальном диапазоне, 18-25) компьютеру, иначе - игроку.
                    if (obj.getName() == _player.getName())
                        _computer.Damage(false);
                    else
                        _player.Damage(false);
                    break;
                case 3:
                    // Исцеление игрока, который делает ход.
                    obj.Hill();
                    break;
                case 4:
                    // Исцеление, задействуется только в случае, когда здоровье игрока <= 35, тем самым повысшая
                    // вероятность на исцеление.
                    obj.Hill();
                    break;
                default:
                    break;
            }
        }
    }
   
}
