﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestTask
{
    class Player
    {
        // Имя игрока.
        string _name;
        // Здоровье игрока.
        int _health;
        // Рандом для определения количества единиц урона/исцеления.
        Random rand;

        // Констуктор, принимающий в качестве параметра имя игрока.
        public Player(string name)
        {
            rand = new Random();
            _name = name;
            _health = 100;
        }

        // Геттер здоровья игрока.
        public int getHealth()
        {
            return _health;
        }

        // Геттер имени игрока.
        public string getName()
        {
            return _name;
        }

        // Метод "Исцеление"
        public void Hill()
        {
            // Рандомно определяем количество единиц исцеления.
            int temp =  rand.Next(18, 25);
            // Если здоровье после исцеления > 100, отнимаем "лишнее".
            if (_health+temp > 100)
                temp = temp - ((_health+temp)-100);
            _health += temp;
            Console.WriteLine();
            Console.WriteLine(_name + " исцелился на " + temp + " единиц. Здоровье = " + _health);
        }

        // Метод "наносящий урон", принимающий значение флага, в каком диапазоне урон.
        public void Damage(bool maxRange = true)
        {
            int temp;
            // если флаг maxRange = true - рандом в диапазоне 10-35, иначе - 18-25.
            if (maxRange)
                temp = rand.Next(10, 35);
            else
                temp = rand.Next(18, 25);
            // Если в результате урона здоровье принимает отрицательное значение, отнимаем лишнее.
            if (_health - temp < 0)
                temp = temp + (_health - temp) ;
            _health -= temp;
            Console.WriteLine();
            Console.WriteLine("Урон " + temp + " единиц. Здоровье " + _name + " = " + _health);
        }


    }
}
