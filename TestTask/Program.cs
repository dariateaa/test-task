﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestTask
{
    class Program
    {
        static void Main(string[] args)
        {
            // Запрашиваем имя игрока.
            Console.WriteLine("Введите своё имя: ");
            string name = Console.ReadLine();
            Game game = new Game(name);
            // Вызов метода Меню.
            game.Menu();
           
        }
    }
}
